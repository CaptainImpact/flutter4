import 'package:flutter/material.dart';

import 'my_drawer.dart';

class SecondPage extends StatelessWidget {
  static const routeName = '/second-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page2'),
      ),
      drawer: MyDrawer(),
    );
  }
}
