import 'package:flutter/material.dart';
import 'package:flutter4/my_drawer.dart';
import 'package:flutter4/page1.dart';
import 'package:flutter4/page2.dart';
import 'package:flutter4/page3.dart';
import 'package:flutter4/page4.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter practice 4',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter practice 4'),

        ),
        drawer: MyDrawer(),
      ),
      initialRoute: '/',
      routes: {
        //'/': (context) => MyApp(),
        FirstPage.routeName:(context) =>FirstPage(),
        SecondPage.routeName:(context) =>SecondPage(),
        ThirdPage.routeName:(context) =>ThirdPage(),
        FourthPage.routeName:(context) =>FourthPage(),
      },
    );
  }
}
