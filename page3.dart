import 'package:flutter/material.dart';

import 'my_drawer.dart';

class ThirdPage extends StatelessWidget {
  static const routeName = '/third-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page3'),
      ),
      drawer: MyDrawer(),
    );
  }
}
