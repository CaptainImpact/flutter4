import 'package:flutter/material.dart';

import 'package:flutter4/page1.dart';
import 'package:flutter4/page2.dart';
import 'package:flutter4/page3.dart';
import 'package:flutter4/page4.dart';

class MyDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Drawer Header'),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          Container(
            height: 600,
            child: FlatButton(
              child: Text('Item 1'),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(FirstPage.routeName);
              },
            ),
          ),
          Container(
            height: 600,
            child: FlatButton(
              child: Text('Item 2'),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(SecondPage.routeName);
              },
            ),
          ),
          Container(
            height: 600,
            child: FlatButton(
              child: Text('Item 3'),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(ThirdPage.routeName);
              },
            ),
          ),
          Container(
            height: 600,
            child: FlatButton(
              child: Text('Item 4'),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(FourthPage.routeName);
              },
            ),
          ),
        ],
      ),
    );
  }

}