import 'package:flutter/material.dart';

import 'my_drawer.dart';

class FirstPage extends StatelessWidget {
  static const routeName = '/first-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page1'),
      ),
      drawer: MyDrawer(),
    );
  }
}
