import 'package:flutter/material.dart';

import 'my_drawer.dart';

class FourthPage extends StatelessWidget {
  static const routeName = '/fourth-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page4'),
      ),
      drawer: MyDrawer(),
    );
  }
}
